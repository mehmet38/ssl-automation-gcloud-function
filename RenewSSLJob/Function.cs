using Google.Cloud.Functions.Framework;
using Google.Cloud.Storage.V1;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using System.IO;
using System.Threading.Tasks;
using System.Diagnostics;
using System;

namespace RenewSSLJob
{
    public class Function : IHttpFunction
    {
        private readonly ILogger<Function> _logger;
        private readonly StorageClient _storageClient;
        private readonly string _domainName;
        private readonly string _bucketName;

        public Function(ILogger<Function> logger)
        {
            _logger = logger;
            _storageClient = StorageClient.Create();
            _domainName = Environment.GetEnvironmentVariable("DOMAIN_NAME");
            _bucketName = Environment.GetEnvironmentVariable("BUCKET_NAME");
        }

        public async Task HandleAsync(HttpContext context)
        {
            string certDir = "/tmp/certbot";
            Directory.CreateDirectory(certDir);

            var processInfo = new ProcessStartInfo
            {
                FileName = "certbot",
                Arguments = $"certonly --standalone --agree-tos --register-unsafely-without-email -d {_domainName} --config-dir {certDir} --logs-dir {certDir} --work-dir {certDir}",
                RedirectStandardOutput = true,
                RedirectStandardError = true,
                UseShellExecute = false,
                CreateNoWindow = true,
            };

            var process = new Process { StartInfo = processInfo };
            process.Start();
            string output = await process.StandardOutput.ReadToEndAsync();
            string error = await process.StandardError.ReadToEndAsync();
            process.WaitForExit();

            if (process.ExitCode != 0)
            {
                _logger.LogError("Certbot error: {Error}", error);
                context.Response.StatusCode = 500;
                await context.Response.WriteAsync("Error generating SSL certificate");
                return;
            }

            _logger.LogInformation("Certbot output: {Output}", output);

            string certPath = Path.Combine(certDir, "live", _domainName);

            foreach (string certFile in new[] { "fullchain.pem", "privkey.pem" })
            {
                string filePath = Path.Combine(certPath, certFile);
                using var fileStream = File.OpenRead(filePath);
                var objectName = $"{_domainName}/{certFile}";
                await _storageClient.UploadObjectAsync(_bucketName, objectName, null, fileStream);
                _logger.LogInformation("Uploaded {FileName} to bucket {BucketName}", certFile, _bucketName);
            }

            await context.Response.WriteAsync("SSL certificates generated and uploaded successfully");
        }
    }

}