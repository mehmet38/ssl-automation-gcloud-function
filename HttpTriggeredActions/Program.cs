using Google.Cloud.Storage.V1;
using Microsoft.OpenApi.Models;

var builder = WebApplication.CreateBuilder(args);

var port = Environment.GetEnvironmentVariable("PORT") ?? "8090";
var target = Environment.GetEnvironmentVariable("TARGET") ?? "World";

// Add services to the container.

builder.Services.AddControllers();
builder.Services.AddSingleton(StorageClient.Create());
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen(config =>
{
    config.SwaggerDoc("v1", new OpenApiInfo
    {
        Title = "CanikoCorp Tools API",
        Version = "v1"
    });
});



var app = builder.Build();

// Ensure the .well-known/acme-challenge directory exists
var challengeDir = Path.Combine(app.Environment.WebRootPath, ".well-known", "acme-challenge");
if (!Directory.Exists(challengeDir))
{
    Directory.CreateDirectory(challengeDir);
}

app.MapGet("/monitoring/status", () => true);

app.UseSwagger();
app.UseSwaggerUI();



app.UseAuthorization();


app.MapControllers();

app.Run($"http://0.0.0.0:{port}");