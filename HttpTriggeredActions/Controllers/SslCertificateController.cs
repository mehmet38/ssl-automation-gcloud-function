﻿using Google.Cloud.Storage.V1;
using Microsoft.AspNetCore.Mvc;
using System.Diagnostics;

namespace HttpTriggeredActions.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class SslCertificateController : ControllerBase
    {
        private readonly ILogger<SslCertificateController> _logger;
        private readonly StorageClient _storageClient;

        public SslCertificateController(ILogger<SslCertificateController> logger, StorageClient storageClient)
        {
            _logger = logger;
            _storageClient = storageClient;
        }

        [HttpPost("renew")]
        public async Task<IActionResult> RenewSsl([FromQuery] string domainName, [FromQuery] string bucketName)
        {
            if (string.IsNullOrEmpty(domainName) || string.IsNullOrEmpty(bucketName))
            {
                return BadRequest("Domain name and bucket name are required.");
            }

            string certDir = "/tmp/certbot";
            Directory.CreateDirectory(certDir);

            var processInfo = new ProcessStartInfo
            {
                FileName = "certbot",
                Arguments = $"certonly --webroot -w /app/wwwroot -d {domainName} --agree-tos --register-unsafely-without-email --no-eff-email",
                RedirectStandardOutput = true,
                RedirectStandardError = true,
                UseShellExecute = false,
                CreateNoWindow = true,
            };

            var process = new Process { StartInfo = processInfo };
            process.Start();
            string output = await process.StandardOutput.ReadToEndAsync();
            string error = await process.StandardError.ReadToEndAsync();
            process.WaitForExit();

            if (process.ExitCode != 0)
            {
                _logger.LogError("Certbot error: {Error}", error);
                return StatusCode(500, "Error generating SSL certificate");
            }

            _logger.LogInformation("Certbot output: {Output}", output);

            string certPath = Path.Combine(certDir, "live", domainName);

            foreach (string certFile in new[] { "fullchain.pem", "privkey.pem" })
            {
                string filePath = Path.Combine(certPath, certFile);
                using var fileStream = System.IO.File.OpenRead(filePath);
                var objectName = $"{domainName}/{certFile}";
                await _storageClient.UploadObjectAsync(bucketName, objectName, null, fileStream);
                _logger.LogInformation("Uploaded {FileName} to bucket {BucketName}", certFile, bucketName);
            }

            return Ok("SSL certificates generated and uploaded successfully");
        }

        [HttpGet(".well-known/acme-challenge/{file}")]
        public IActionResult GetChallengeFile(string file)
        {
            var filePath = Path.Combine("wwwroot/.well-known/acme-challenge", file);
            if (System.IO.File.Exists(filePath))
            {
                return PhysicalFile(filePath, "text/plain");
            }
            return NotFound();
        }
    }
}
